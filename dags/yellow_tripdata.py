from prefect import Flow, Parameter
from prefect.tasks.secrets import EnvVarSecret

from .expectations.yellow_tripdata import schema
from .tasks.convert import ConvertSource
from .tasks.download import DownloadSource
from .tasks.ingest import IngestSource, PrepareIngest
from .tasks.validate import ValidateSource

download_source = DownloadSource(
    url="https://s3.amazonaws.com/nyc-tlc/trip+data/yellow_tripdata_{year}-{month:02}.csv",
    data_dir="data/raw/yellow",
)

validate_source = ValidateSource(
    schema=schema,
)

convert_source = ConvertSource(
    data_dir="data/jsonl/yellow",
)

prepare_ingest = PrepareIngest(
    schema="source",
    table="yellow_tripdata",
)

ingest_source = IngestSource()

with Flow("yellow_tripdata") as yellow_tripdata_flow:
    dsn = EnvVarSecret("DATABASE")

    year = Parameter("year", default=2020)
    month = Parameter("month", default=1)

    downloaded_filename = download_source(year, month)
    validated_filename = validate_source(downloaded_filename)
    converted_filename = convert_source(validated_filename)
    ingest_statement = prepare_ingest(dsn)
    ingest_source(
        dsn=dsn, ingest_statement=ingest_statement, filename=converted_filename
    )
