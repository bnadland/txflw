WITH source AS (
  SELECT * FROM {{ source("source", "yellow_tripdata") }}
),

renamed AS (
  SELECT
    data->>'VendorID'::TEXT                                               AS vendor_id,
    TO_TIMESTAMP(data->>'tpep_pickup_datetime', 'YYYY-MM-DD HH24:MI:SS')  AS pickedup_at,
    TO_TIMESTAMP(data->>'tpep_dropoff_datetime', 'YYYY-MM-DD HH24:MI:SS') AS droppedoff_at,
    data->>'store_and_fwd_flag'::TEXT                                     AS stored_and_forwarded,
    data->>'RatecodeID'::TEXT                                             AS ratecode_id,
    data->>'PULocationID'::TEXT                                           AS pickup_location_id,
    data->>'DOLocationID'::TEXT                                           AS dropoff_location_id,
    NULLIF(data->>'passenger_count', '')::INTEGER                         AS passengers,
    NULLIF(data->>'trip_distance', '')::NUMERIC                           AS miles,
    NULLIF(data->>'fare_amount', '')::NUMERIC                             AS fare,
    NULLIF(data->>'extra', '')::NUMERIC                                   AS extra_surcharge,
    NULLIF(data->>'mta_tax', '')::NUMERIC                                 AS mta_tax,
    NULLIF(data->>'tip_amount', '')::NUMERIC                              AS tip,
    NULLIF(data->>'tolls_amount', '')::NUMERIC                            AS tolls,
    NULLIF(data->>'ehail_fee', '')::NUMERIC                               AS ehail_fee,
    NULLIF(data->>'improvement_surcharge', '')::NUMERIC                   AS improvement_surcharge,
    NULLIF(data->>'total_amount', '')::NUMERIC                            AS total,
    data->>'payment_type'::TEXT                                           AS payment_type_id,
    data->>'trip_type'::TEXT                                              AS trip_type_id,
    NULLIF(data->>'congestion_surcharge', '')::NUMERIC                    AS congestion_surcharge
  FROM source
)

SELECT * FROM renamed
