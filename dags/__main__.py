from dotenv import load_dotenv
from typer import Typer

from .green_tripdata import green_tripdata_flow
from .yellow_tripdata import yellow_tripdata_flow

cli = Typer()

@cli.command()
def main():
    load_dotenv(verbose=True)
    year = 2019
    for month in [1]:
        print(f"\nprocessing {year}-{month}...\n")

        green_tripdata_flow.run(
            parameters=dict(
                year=year,
                month=month,
            )
        )
        yellow_tripdata_flow.run(
            parameters=dict(
                year=year,
                month=month,
            )
        )


if __name__ == "__main__":
    cli()
