# txflw

End to end project to show a possible solution for:

* Data fetching from S3
* Data validation with [pandera](https://pandera.readthedocs.io/en/stable/)
* Quick ingestion into postgres
* Transformation with [dbt](https://docs.getdbt.com/) for datawarehouse and reporting tables
* Integrity checks in dbt
* datawarehouse schema documentation

Source data from: https://www1.nyc.gov/site/tlc/about/tlc-trip-record-data.page


## Requirements

* [pyenv](https://github.com/pyenv/pyenv)
* [poetry](https://python-poetry.org/)

## Setup

* `docker-compose up` to get a postgres instance up and running (if already available this can be skipped)
* Add `.env` file with DATABASE env for the postgresql dsn (in case of docker-compose this would be: `DATABASE="postgresql://dwh:dwh@localhost:5432/dwh"`)
* `poetry install` to install all python dependencies
* `poetry run task setup` to run both dags and dbt

## Documentation

Currently only dbt documentation is done and can be generated and opened with `poetry run task docs`

## Tests

Tests can be run with `poetry run task test` and include coverage and flake8, flake8 in turn check with black and isort as well.

## Formatting/linting

Code in `tests` and `dags` can be formatted and linted with `poetry run task lint`

## Tasks

Tasks are defined with `taskipy` in `pyproject.toml`, see `poetry run task --list` for a list of currently defined tasks.

## License 

MIT 
