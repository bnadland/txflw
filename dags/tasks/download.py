from pathlib import Path
from urllib.request import urlretrieve

from prefect import Task
from prefect.engine.signals import SKIP


class DownloadSource(Task):
    def __init__(self, data_dir: str, url: str, **kwargs):
        self.url = url
        self.data_dir = Path(data_dir).resolve()
        self.data_dir.mkdir(parents=True, exist_ok=True)
        super().__init__(**kwargs)

    def run(self, year: int, month: int) -> str:
        url = self.url.format(year=year, month=month)
        dest = self.data_dir.joinpath(Path(url).name)
        if dest.is_file():
            raise SKIP(message=f"{dest} already exists")
        urlretrieve(url, dest)
        return str(dest)
