WITH unioned AS (
  SELECT *, 'yellow'::text AS trip_type FROM {{ ref("stg_yellow_tripdata") }}
  UNION ALL
  SELECT *, 'green'::text AS trip_type  FROM {{ ref("stg_green_tripdata") }}
),

payment_type AS (
  SELECT * FROM {{ ref("payment_type") }}
),

ratecode AS (
  SELECT * FROM {{ ref("ratecode") }}
),

tripdata AS (
  SELECT
    unioned.pickedup_at         AS pickedup_at,
    unioned.droppedoff_at       AS dropped_off,
    unioned.miles               AS miles,
    unioned.total               AS total,
    unioned.pickup_location_id  AS pickup_location,
    unioned.dropoff_location_id AS dropoff_location,
    ratecode.name               AS ratecode,
    payment_type.name           AS payment_type
  FROM unioned
  LEFT JOIN ratecode     ON unioned.ratecode_id = ratecode.ratecode_id
  LEFT JOIN payment_type ON unioned.payment_type_id = payment_type.payment_type_id
)

SELECT * FROM tripdata
