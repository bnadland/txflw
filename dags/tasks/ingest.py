from prefect import Task
from psycopg2 import connect
from psycopg2.sql import SQL, Identifier

CREATE_SCHEMA = """
CREATE SCHEMA IF NOT EXISTS {schema}
"""

CREATE_TABLE = """
CREATE TABLE IF NOT EXISTS {schema}.{table} (
  ingested_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  data        JSONB     NOT NULL
)
"""

COPY_FROM = """
COPY {schema}.{table} (data) FROM STDIN
"""


class PrepareIngest(Task):
    def __init__(self, table: str, schema: str = "source", **kwargs):
        self.table = Identifier(table)
        self.schema = Identifier(schema)
        super().__init__(**kwargs)

    def create_schema(self):
        return SQL(CREATE_SCHEMA).format(schema=self.schema)

    def create_table(self):
        return SQL(CREATE_TABLE).format(schema=self.schema, table=self.table)

    def copy_from(self):
        return SQL(COPY_FROM).format(schema=self.schema, table=self.table)

    def run(self, dsn: str) -> SQL:
        con = connect(dsn)
        with con.cursor() as cursor:
            cursor.execute(self.create_schema())
            cursor.execute(self.create_table())
        con.commit()
        con.close()
        return self.copy_from()


class IngestSource(Task):
    def run(self, dsn: str, ingest_statement: SQL, filename: str):
        con = connect(dsn)
        with con.cursor() as cursor:
            with open(filename, "r") as f:
                cursor.copy_expert(ingest_statement, f)
        con.commit()
        con.close()
