from pathlib import Path

from pandas import read_csv
from prefect import Task
from prefect.engine.signals import SKIP


class ConvertSource(Task):
    def __init__(self, data_dir: str, **kwargs):
        self.data_dir = Path(data_dir).resolve()
        self.data_dir.mkdir(parents=True, exist_ok=True)
        super().__init__(**kwargs)

    def run(self, filename: str) -> str:
        dest = self.data_dir.joinpath(f"{Path(filename).stem}.jsonl")
        if dest.is_file():
            raise SKIP(message=f"{dest} already exists")
        read_csv(filename, low_memory=False).to_json(dest, orient="records", lines=True)
        return dest
