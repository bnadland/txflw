from pandas import read_csv
from pandera import DataFrameSchema
from prefect import Task


class ValidateSource(Task):
    def __init__(self, schema: DataFrameSchema, **kwargs):
        self.schema = schema
        super().__init__(**kwargs)

    def run(self, filename: str) -> str:
        self.schema.validate(read_csv(filename, low_memory=False))
        return filename
